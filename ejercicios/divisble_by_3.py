import doctest
def divisible_by_3(num):
    """
    3: (Task 0.5.3) Divisibility
    >>> divisible_by_3(9)
    True
    >>> divisible_by_3(7)
    False
    """
    if(num%3 == 0):
        return True
    else:
        return False 

if __name__ == "__main__":
    doctest.testmod()

