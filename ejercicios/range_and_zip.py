import doctest

def range_and_zip(letters):
    """
    19.(Task 0.5.19) Using range and zip
    >>> range_and_zip("ABCDE")
    [(0, 'A'), (1, 'B'), (2, 'C'), (3, 'D'), (4, 'E')]
  
    Do not use a list comprehension use range and zip
    """
    i=range(len(letters))
    return zip(i, letters)


if __name__ == "__main__":
    doctest.testmod()
