import doctest
def predict_expresion(x,y, prediction):
	"""
    4: (Task 0.5.4) Conditional Expression
    Try to predict the value of 2**(y+1/2) if x+10<0 else 2**(y-1/2)
    >>> predict_expresion(-9, 1/2, 1)
    1
    """
	if (x+10)<0:
		return 2**(y+1/2)
	else:
		return 2**(y-1/2)

if __name__ == "__main__":
	doctest.testmod()
