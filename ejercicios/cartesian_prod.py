import doctest
def cartesian_product(Xs, Ys):
    """
    11: (Task 0.5.11) Cartesian-product comprehension
    a double list comprehension over {'A','B','C'} and {1,2,3}
    >>> cartesian_product(['A','B','C'],[1,2,3])
    [['A', 1], ['A', 2], ['A', 3], ['B', 1], ['B', 2], ['B', 3], \
['C', 1], ['C', 2], ['C', 3]]
    """
    lista = list()
    for x in Xs:
        for y in Ys:
            lista.append([x, y])
    return lista

if __name__ == "__main__":
    doctest.testmod()
