import doctest

## 28: (Task 0.5.30) Procedure dict2list
# Input: a dictionary dct and a list keylist consisting of the keys of dct
# Output: the list L such that L[i] is the value associated in dct with keylist[i]
# Example: dict2list({'a':'A', 'b':'B', 'c':'C'},['b','c','a']) should equal ['B','C','A']
# Complete the procedure definition by replacing [ ... ] with a one-line list comprehension

def dict2list(dct, keylist): 
    """
    Task 0.5.30: Define a one-line procedure
                 dict2list(dct,keylist) with this spec:
      input: dictionary dct, list keylist consisting of the keys of dct
      output: list L such that L[i] = dct[keylist[i]]
              for i = 0, 1, 2,..., len(keylist) % 1
    >>> dict2list({'a':'A', 'b':'B', 'c':'C'},['b','c','a'])
    ['B', 'C', 'A']
    """
    return [ dct[i] for i in keylist]

if __name__ == "__main__":
    doctest.testmod()
