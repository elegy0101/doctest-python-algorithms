import doctest

def list_sum_zip(A,B):
    """
    (Task 0.5.20) Using zip to find elementwise sums
    A one-line comprehension that uses zip together with the variables A and B.
    The comprehension should evaluate to a list whose ith element is the ith element of
    A plus the ith element of B.
    >>> list_sum_zip([10,20,30],[1,2,3])
    [11, 22, 33]
    """
    return [(i+j) for i, j in zip(A, B)]

if __name__ == "__main__":
    doctest.testmod()


