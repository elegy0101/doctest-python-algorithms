import doctest

def square_dict(n):
    """
    (Task 0.5.23) A dictionary mapping integers to their squares
    Replace {...} with a one-line dictionary comprehension
    >>> square_dict(10)
    {0: 0, 1: 1, 2: 4, 3: 9, 4: 16, 5: 25, 6: 36, 7: 49, 8: 64, 9: 81}
    """

    square_dict={}
    for i in range(n):
        square_dict={i:i**2}
    return square_dict 

    i = 0
    dict = {}
    while i < n:
        x= i * i
        dict[i] = x
        i = i + 1
    return dict


if __name__ == "__main__":
    doctest.testmod()
