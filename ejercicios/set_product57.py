import doctest
def set_product57(xs, ys):
    """
    7: (Task 0.5.7) Double comprehension evaluating to nine-element set
    Return a set containing the multiplication of every element in a set multiplied by the other
    >>> set_product57({1,2,3},{3,4,5})
    set([3, 4, 5, 6, 8, 9, 10, 12, 15])
    """
    lista = []
    for i in xs:
        for j in ys:
            lista.append(i*j)
    return set(lista)
    
if __name__ == "__main__":
    doctest.testmod()

