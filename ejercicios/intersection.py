import doctest
def intersection(Ss, Ts):
    """
    9: (Task 0.5.9) Set intersection as a comprehension
    >>> intersection({1, 2, 3, 4},{3, 4, 5, 6})
    set([3, 4])
    """
    
    return Ss & Ts
    
if __name__ == "__main__":
    doctest.testmod()

