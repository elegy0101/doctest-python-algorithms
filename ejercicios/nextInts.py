import doctest
def nextInts(L):
    """
    Task 0.5.28: Define a one-line procedure nextInts(L) specified as follows:
      input: list L of integers
      output: list of integers whose ith element is one more than the i
      th element of L
    >>> nextInts([1, 5, 7])
    [2, 6, 8]
    """
    return [x + 1 for x in L]

if __name__ == "__main__":
    doctest.testmod()
