import doctest

def value_list_modified(k, dlist):
    """
    Task 0.5.22: Modify the comprehension in Task 0.5.21 to handle the case in which k
    might not appear in all the dictionaries. The comprehension evaluates to the list whose ith
    element is the value corresponding to key k in the i
    th dictionary in dlist if that dictionary
    contains that key, and 'NOT PRESENT' otherwise.
    >>> value_list_modified('Bilbo',[{'Bilbo':'Ian','Frodo':'Elijah'},{'Bilbo':'Martin','Thorin':'Richard'}])
    ['Ian', 'Martin']
    >>> value_list_modified('Frodo',[{'Bilbo':'Ian','Frodo':'Elijah'},{'Bilbo':'Martin','Thorin':'Richard'}])
    ['Elijah', 'NOT PRESENT']
    """
    
    val=[]
    for i in dlist:
        if(k in i):
    	    val.append(i[k])
        else:
            val.append('NOT PRESENT')
    return val

if __name__ == "__main__":
    doctest.testmod()
    
    
