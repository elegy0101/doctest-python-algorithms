import doctest
def odd_num_list(n):
    """
    Task 0.5.18: Write a comprehension over a range of the form range(n) such that the
    value of the comprehension is the set of odd numbers from 1 to n.
    >>> odd_num_list(17)
    [1, 3, 5, 7, 9, 11, 13, 15]
    """
    lista = []
    for i in range(1, n):
        if(i%2 != 0):
            lista.append(i)
    
    return lista
   
if __name__ == "__main__":
    doctest.testmod()

