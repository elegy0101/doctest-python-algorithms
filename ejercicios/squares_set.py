import doctest
def squares_set(numbers):
    """
    5: (Task 0.5.5) Squares Set Comprehension
    Given a set of numbers return a set of the numbers squared
    >>> squares_set({1,2,3,4,5,5,5})
    set([16, 1, 4, 25, 9])
    """
    lista = []
    for i in numbers:
        lista.append(i**2)
    return set(lista)
    
if __name__ == "__main__":
    doctest.testmod()

