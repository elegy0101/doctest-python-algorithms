import doctest
def first_zero_sum_list(list_of_numbers):
    """
    16: (Task 0.5.16) first nontrivial three-element tuple summing to zero
    >>> first_zero_sum_list([-4, -2, 1, 2, 5, 0])
    [(-4, 2, 2)]
    """
    suma = 0
    lista = []
    bandera = False
    for i in list_of_numbers:
        if(bandera):
            break
        for j in list_of_numbers:
            if(bandera):
                break
            for k in list_of_numbers:
                if(bandera):
                    break
                if(i != 0 or j != 0 or k != 0):
                    suma = i + j + k
                    if(suma == 0):
                        lista.append((i,j,k))
                        bandera = True
        suma = 0
    return lista
    
if __name__ == "__main__":
    doctest.testmod()
    
