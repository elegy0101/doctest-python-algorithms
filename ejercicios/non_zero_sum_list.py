import doctest
def non_zero_sum_list(list_of_numbers):
    """
    Task 0.5.15: Modify the comprehension of the previous task so that the resulting list does
    not include (0, 0, 0). Hint: add a filter.
    >>> non_zero_sum_list([-4, -2, 1, 2, 5, 0])
    [(-4, 2, 2), (-2, 1, 1), (-2, 2, 0), (-2, 0, 2), (1, -2, 1), (1, 1, -2), (2, -4, 2), (2, -2, 0), (2, 2, -4), (2, 0, -2), (0, -2, 2), (0, 2, -2)]
    """
    suma = 0
    lista = []
    for i in list_of_numbers:
        for j in list_of_numbers:
            for k in list_of_numbers:
                if(i != 0 or j != 0 or k != 0):
                    suma = i + j + k
                    if(suma == 0):
                        lista.append((i,j,k))
        suma = 0
    return lista
    
if __name__ == "__main__":
    doctest.testmod()

