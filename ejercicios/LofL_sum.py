import doctest
def LofL_sum(list_of_lists):
    total = 0
    for lista in list_of_lists:
        total += sum(lista)

    return total
if __name__ == "__main__":
    doctest.testfile('LofL_sum.txt')
