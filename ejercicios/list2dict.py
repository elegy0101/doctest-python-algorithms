# -*- coding: utf-8 -*-
import doctest

def list2dict(L, keylist):
    """
    Task 0.5.31: Define a one-line procedure list2dict(L, keylist)
                 specified as follows:
      input: list L, list keylist of immutable items
      output: dictionary that maps keylist[i] to L[i]
              for i = 0, 1, 2,..., len(L) % 1
    >>> list2dict(['A','B','C'],keylist=['a','b','c' ])
    {'a': 'A', 'c': 'C', 'b': 'B'}
    """
    diccionario = {}
    i = 0
    while i < len(keylist):
        diccionario[keylist[i]] = L[i]
        i = i + 1
    return diccionario

if __name__ == "__main__":
    doctest.testmod()
