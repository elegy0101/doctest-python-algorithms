import doctest

def pows_two(numbers):
    """
    6: (Task 0.5.6) Powers-of-2 Set Comprehension
    Given a set of numbers return the powers of two of those numbers
    >>> pows_two({0,1,2,3,4})
    set([0, 1, 4, 16, 9])
    """
    cuadrados = []
    for x in numbers:
        cuadrados.append(x**2)
    return set(cuadrados)

if __name__ == "__main__":
    doctest.testmod()
