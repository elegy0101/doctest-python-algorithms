import doctest
def is_element_repeated(things):
    """
    17: Task 0.5.17: Find an example of a list L such that len(L) and len(list(set(L))) are diferent."
    >>> is_element_repeated([1,1,2,3])
    True
    >>> is_element_repeated([1,2,3,4])
    False
    """
    return bool(len(things) != len(set(things)))

if __name__ == "__main__":
    doctest.testmod()
